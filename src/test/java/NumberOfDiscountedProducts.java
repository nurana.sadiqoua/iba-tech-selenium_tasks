import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class NumberOfDiscountedProducts {

    @Test
    void countDiscounts() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);

        try {
            webDriver.get("https://www.etsy.com/");
            Actions actions = new Actions(webDriver);

            WebElement clothesAndShoes = webDriver.findElement(By.cssSelector("#desktop-category-nav > div.wt-bg-white" +
                    ".wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-" +
                    "bar > div > ul > li:nth-child(2)"));
            actions.moveToElement(clothesAndShoes).perform();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#side-nav-category-link-10936")));
            WebElement mens = webDriver.findElement(By.cssSelector("#side-nav-category-link-10936"));
            actions.moveToElement(mens).perform();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#catnav-l4-11109")));
            WebElement boots = webDriver.findElement(By.cssSelector("#catnav-l4-11109"));
            actions.moveToElement(boots).perform();
            boots.click();

            List<WebElement> webElementListOfDiscountedShoes = webDriver.findElements(By.cssSelector("#content > div " +
                    "> div.wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-" +
                    "bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings" +
                    "-group > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div >" +
                    " ul > li > div > a > div.v2-listing-card__info > div > span > span.text-body-smaller.promotion-price.normal.no-wrap"));
            System.out.println(webElementListOfDiscountedShoes.size());

            List<WebElement> webElementListOfAllShoes = webDriver.findElements(By.cssSelector("#content > div > div." +
                    "wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-" +
                    "1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings-group" +
                    " > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div > ul > " +
                    "li > div > a > div.v2-listing-card__info > div > span"));
            System.out.println(webElementListOfAllShoes.size());

            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } finally {
            webDriver.close();
        }
    }
}
