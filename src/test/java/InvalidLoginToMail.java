import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InvalidLoginToMail {

    @Test
    void confirmInvalidLogin() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        try {
            webDriver.get("https://mail.ru/");

            WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.sv" +
                    "elte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")));
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1e" +
                    "yrl7y > div.email-input-container.svelte-1eyrl7y > input")).sendKeys("ibatechtest");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y")).click();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.sv" +
                    "elte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")));
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container" +
                    ".svelte-1eyrl7y > input")).sendKeys("nurane777");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y")).click();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.error.svelte-1eyrl7y")));
            assertTrue(webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.error.svelte-1eyrl7y")).isDisplayed());

            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } finally {
            webDriver.close();
        }
    }
}
