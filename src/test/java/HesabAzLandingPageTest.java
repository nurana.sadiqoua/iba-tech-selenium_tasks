import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HesabAzLandingPageTest {

    @Test
    void checkTitleChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        String expectedTitle = "Hesab.az : Onlayn Ödə | Azərbaycanda ən böyük onlayn ödəmə sistemi";
        try {
            webDriver.get("https://hesab.az");

            webDriver.findElement(By.cssSelector("body > app-root > app-landing > section.merchants-panel > div > div > ul > li:nth-child(1) > a")).click();
            webDriver.findElement(By.cssSelector("body > app-root > div:nth-child(3) > app-merchants > app-masterpass-march-popup > app-popup > div > div > div > button > img")).click();

            List<WebElement> webElements = webDriver.findElements(By.cssSelector("body > app-root > div:nth-child(3) > app-merchants > section > div > ul > li > a"));

            assertEquals(5, webElements.size());

            //Thread.sleep(5000);

        //} catch (InterruptedException e) {
            //e.printStackTrace();
        } finally {
            webDriver.close();
        }
    }
}
