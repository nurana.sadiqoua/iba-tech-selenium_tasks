import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SendingMail {

    @Test
    void confirmMailIsSent() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        try {
            webDriver.get("https://mail.ru/");

            WebDriverWait webDriverWait = new WebDriverWait(webDriver, 100);

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.sv" +
                    "elte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")));
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1e" +
                    "yrl7y > div.email-input-container.svelte-1eyrl7y > input")).sendKeys("ibatechtest");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y")).click();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.sv" +
                    "elte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")));
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container" +
                    ".svelte-1eyrl7y > input")).sendKeys("nurane77");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y")).click();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#app-canvas > div > div." +
                    "application-mail > div.application-mail__overlay > div > div.application-mail__layout.application" +
                    "-mail__layout_main > span > div.layout__column.layout__column_left > div.layout__column-wrapper " +
                    "> div > div > div > div:nth-child(1) > div > div > a > span")));
            webDriver.findElement(By.cssSelector("#app-canvas > div > div.application-mail > div.application-mail__o" +
                    "verlay > div > div.application-mail__layout.application-mail__layout_main > span > div.layout__" +
                    "column.layout__column_left > div.layout__column-wrapper > div > div > div > div:nth-child(1) > " +
                    "div > div > a > span")).click();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > div.compose-wind" +
                    "ows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window." +
                    "compose-app_adaptive > div > div.compose-app__compose > div.container--rp3CE > div.scrollview--S" +
                    "iHhk.scrollview_main--3Vfg9 > div.head_container--3W05z > div > div > div.wrap--2sfxq > div > di" +
                    "v.contacts--1ofjA > div > div")));

            webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.c" +
                    "ompose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app" +
                    "__compose > div.container--rp3CE > div.scrollview--SiHhk.scrollview_main--3Vfg9 > div.head_conta" +
                    "iner--3W05z > div > div > div.wrap--2sfxq > div > div.contacts--1ofjA > div > div > label > div " +
                    "> div > input")).sendKeys("nurana.sadiqova9@gmail.com");

            webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.c" +
                    "ompose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app" +
                    "__compose > div.container--rp3CE > div.scrollview--SiHhk.scrollview_main--3Vfg9 > div.subject__c" +
                    "ontainer--HWnat > div.subject__wrapper--2mk6m > div.container--3QXHv > div > input")).sendKeys("TEST");

            webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.c" +
                    "ompose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app" +
                    "__footer > div.compose-app__buttons > span.button2.button2_base.button2_primary.button2_hover-su" +
                    "pport.js-shortcut > span > span")).click();
            webDriver.findElement(By.cssSelector("body > div.overlay--2THpd > div > div > div.wrapper-1-2-17 > button" +
                    ".base-1-2-63.base-d5-1-2-94.primary-1-2-77.auto-1-2-89")).click();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > div:nth-child(28) > div > div > div.layer-window__container > div.layer-window__block > div > div > div.layer__header > a")));
            assertTrue(webDriver.findElement(By.cssSelector("body > div:nth-child(28) > div > div > div.layer-window__container > div.layer-window__block > div > div > div.layer__header > a")).isDisplayed());

            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } finally {
            webDriver.close();
        }
    }
}
