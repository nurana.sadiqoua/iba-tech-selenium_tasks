import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CountItemsTest {

    @Test
    void countPHPTraveler() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        try {

            webDriver.get("https://www.phptravels.net/admin");
            webDriver.findElement(By.cssSelector("body > div:nth-child(6) > form.form-signin.form-horizontal.wow." +
                    "fadeIn.animated.animated > div:nth-child(1) > label:nth-child(2) > input[type=text]"))
                    .sendKeys("admin@phptravels.com");
            webDriver.findElement(By.cssSelector("body > div:nth-child(6) > form.form-signin.form-horizontal.wow." +
                    "fadeIn.animated.animated > div:nth-child(1) > label:nth-child(3) > input[type=password]"))
                    .sendKeys("demoadmin");
            webDriver.findElement(By.cssSelector("body > div:nth-child(6) > form.form-signin.form-horizontal.wow." +
                    "fadeIn.animated.animated > button")).click();

            WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#social-sidebar-menu > li")));

            List<WebElement> webElementList = webDriver.findElements(By.cssSelector("#social-sidebar-menu > li"));

            assertEquals(21, webElementList.size());

            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        } finally {
            webDriver.close();
        }
    }

}
