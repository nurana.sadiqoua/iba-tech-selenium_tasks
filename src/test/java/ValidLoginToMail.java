import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidLoginToMail {

    @Test
    void confirmValidLogin() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        try {
            webDriver.get("https://mail.ru/");

            WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.sv" +
                    "elte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")));
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1e" +
                    "yrl7y > div.email-input-container.svelte-1eyrl7y > input")).sendKeys("ibatechtest");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y")).click();

            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.sv" +
                    "elte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")));
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container" +
                    ".svelte-1eyrl7y > input")).sendKeys("nurane77");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y")).click();


            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#portal-headline > tab" +
                    "le > tbody > tr > td.w-x-ph__col.w-x-ph__col_left > a.x-ph__link.x-ph__link_first")));
            assertEquals("Mail.ru", webDriver.findElement(By.cssSelector("#portal-headline > table > tbody >" +
                    " tr > td.w-x-ph__col.w-x-ph__col_left > a.x-ph__link.x-ph__link_first")).getText());

            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } finally {
            webDriver.close();
        }
    }
}
