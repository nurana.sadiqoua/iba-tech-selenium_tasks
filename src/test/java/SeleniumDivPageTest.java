import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SeleniumDivPageTest {

    @Test
    void goToSeleniumPage() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        try {
            webDriver.get("https://www.google.com");
            webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1)" +
                    " > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input")).sendKeys("Selenium");
            webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1)" +
                    " > div.A8SBwf > div.FPdoLc.tfB0Bf > center > input.gNO89b")).click();
            webDriver.findElement(By.cssSelector("#rso > div:nth-child(1) > div > div > div > div.yuRUbf > a > h3")).click();

            assertEquals("https://www.selenium.dev/", webDriver.getCurrentUrl());

            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        } finally {
            webDriver.close();
        }
    }
}
