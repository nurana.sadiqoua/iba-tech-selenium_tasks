import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PhpTravelersValidLoginTest {

    @Test
    void validLogin() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        try {
            webDriver.get("https://www.phptravels.net/admin");
            webDriver.findElement(By.cssSelector("body > div:nth-child(6) > form.form-signin.form-horizontal.wow." +
                    "fadeIn.animated.animated > div:nth-child(1) > label:nth-child(2) > input[type=text]"))
                    .sendKeys("admin@phptravels.com");
            webDriver.findElement(By.cssSelector("body > div:nth-child(6) > form.form-signin.form-horizontal.wow." +
                    "fadeIn.animated.animated > div:nth-child(1) > label:nth-child(3) > input[type=password]"))
                    .sendKeys("demoadmin");
            webDriver.findElement(By.cssSelector("body > div:nth-child(6) > form.form-signin.form-horizontal.wow." +
                    "fadeIn.animated.animated > button")).click();

            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();
        }
    }
}
