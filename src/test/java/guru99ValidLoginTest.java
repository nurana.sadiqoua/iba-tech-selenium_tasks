import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class guru99ValidLoginTest {

    @Test
    void validLogin() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\nuran\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        try {
            webDriver.get("http://www.demo.guru99.com/V4/");

            webDriver.findElement(By.cssSelector("body > form > table > tbody > tr:nth-child(1) > td:nth-child(2) " +
                    "> input[type=text]")).sendKeys("mngr318736");
            webDriver.findElement(By.cssSelector("body > form > table > tbody > tr:nth-child(2) > td:nth-child(2) " +
                    "> input[type=password]")).sendKeys("pAhabAh");
            webDriver.findElement(By.cssSelector("body > form > table > tbody > tr:nth-child(3) > td:nth-child(2) " +
                    "> input[type=submit]:nth-child(1)")).click();

            Assertions.assertEquals("Welcome To Manager's Page of Guru99 Bank", webDriver.
                    findElement(By.cssSelector("body > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td > marquee")).getText());

            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();
        }
    }
}
